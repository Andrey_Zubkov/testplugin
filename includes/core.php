<?php

add_action( 'woocommerce_product_options_advanced', 'wc_rrp_product_field' );

function wc_rrp_product_field() {
    global $product, $post;
    $_product = wc_get_product( $post->ID );
   // var_dump($_product); exit;
    $type =  $_product->get_type();
    if($type == 'simple') :
?>
        <h1><strong>AMINO ACID PROFILE</strong></h1>
        <table class="table table-striped">
            <thead>
            <tr>
                <th></th>
                <th><strong>Per 100g</strong></th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Alanine</td>
                <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom[_product_alanine]"
                           value="<?php echo get_post_meta( $post->ID, '_product_alanine', true ); ?>"
                           style="width: 15.75%;margin-right: 2%;"/>mg</td>
            </tr>
            <tr>
                <td>Arginine</td>
                <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom[_product_arginine]"
                           value="<?php echo get_post_meta( $post->ID, '_product_arginine', true ); ?>"
                           style="width: 15.75%;margin-right: 2%;"/>mg</td>
            </tr>
            <tr>
                <td>Cysteine</td>
                <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom[_cysteine]"
                           value="<?php echo get_post_meta( $post->ID, '_cysteine', true ); ?>"
                           style="width: 15.75%;margin-right: 2%;"/>mg</td>
            </tr>
            <tr>
                <td>Glutamic Acid</td>
                <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom[_glutamic_acid]"
                           value="<?php echo get_post_meta( $post->ID, '_glutamic_acid', true ); ?>"
                           style="width: 15.75%;margin-right: 2%;"/>mg</td>
            </tr>
            <tr>
                <td>Glycine</td>
                <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom[_glycine]"
                           value="<?php echo get_post_meta( $post->ID, '_glycine', true ); ?>"
                           style="width: 15.75%;margin-right: 2%;"/>mg</td>
            </tr>
            <tr>
                <td>Histidine</td>
                <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom[_histidine]"
                           value="<?php echo get_post_meta( $post->ID, '_histidine', true ); ?>"
                           style="width: 15.75%;margin-right: 2%;"/>mg</td>
            </tr>
            <tr>
                <td>Isoleucine</td>
                <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom[_isoleucine]"
                           value="<?php echo get_post_meta( $post->ID, '_isoleucine', true ); ?>"
                           style="width: 15.75%;margin-right: 2%;"/>mg</td>
            </tr>
            <tr>
                <td>Leucine</td>
                <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom[_leucine]"
                           value="<?php echo get_post_meta( $post->ID, '_leucine', true ); ?>"
                           style="width: 15.75%;margin-right: 2%;"/>mg</td>
            </tr>
            <tr>
                <td>Lysine</td>
                <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom[_lysine]"
                           value="<?php echo get_post_meta( $post->ID, '_lysine', true ); ?>"
                           style="width: 15.75%;margin-right: 2%;"/>mg</td>
            </tr>
            <tr>
                <td>Methionine</td>
                <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom[_methionine]"
                           value="<?php echo get_post_meta( $post->ID, '_methionine', true ); ?>"
                           style="width: 15.75%;margin-right: 2%;"/>mg</td>
            </tr>
            <tr>
                <td>Phenylalanine</td>
                <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom[_phenylalanine]"
                           value="<?php echo get_post_meta( $post->ID, '_phenylalanine', true ); ?>"
                           style="width: 15.75%;margin-right: 2%;"/>mg</td>
            </tr>
            <tr>
                <td>Proline</td>
                <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom[_proline]"
                           value="<?php echo get_post_meta( $post->ID, '_proline', true ); ?>"
                           style="width: 15.75%;margin-right: 2%;"/>mg</td>
            </tr>
            <tr>
                <td>Serine</td>
                <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom[_serine]"
                           value="<?php echo get_post_meta( $post->ID, '_serine', true ); ?>"
                           style="width: 15.75%;margin-right: 2%;"/>mg</td>
            </tr>
            <tr>
                <td>Threonine</td>
                <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom[_threonine]"
                           value="<?php echo get_post_meta( $post->ID, '_threonine', true ); ?>"
                           style="width: 15.75%;margin-right: 2%;"/>mg</td>
            </tr>
            <tr>
                <td>Tryptophan</td>
                <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom[_tryptophan]"
                           value="<?php echo get_post_meta( $post->ID, '_tryptophan', true ); ?>"
                           style="width: 15.75%;margin-right: 2%;"/>mg</td>
            </tr>
            <tr>
                <td>Tyrosine</td>
                <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom[_tyrosine]"
                           value="<?php echo get_post_meta( $post->ID, '_tyrosine', true ); ?>"
                           style="width: 15.75%;margin-right: 2%;"/>mg</td>
            </tr>
            <tr>
                <td>Valine</td>
                <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom[_valine]"
                           value="<?php echo get_post_meta( $post->ID, '_valine', true ); ?>"
                           style="width: 15.75%;margin-right: 2%;"/>mg</td>
            </tr>
            </tbody>
        </table>
        <h1><strong>SUPPLEMENT FACTS</strong></h1>
        <p><strong>Serving Size: </strong>40.62 g(Approx. 2 Scoops)</p>
        <p><strong>Serving Per Container: </strong>24</p>
        <table class="table table-striped">
            <thead>
            <tr>
                <th></th>
                <th>Amount Per Serving</th>
                <th><strong>%DV</strong></th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Calories</td>
                <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_2[_calories]"
                           value="<?php echo get_post_meta( $post->ID, '_calories', true ); ?>"
                           style="width: 15.75%;margin-right: 2%;"/></td>
                <td></td>
            </tr>
            <tr>
                <td>Calories from Fat</td>
                <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_2[_calories_fat]"
                           value="<?php echo get_post_meta( $post->ID, '_calories_fat', true ); ?>"
                           style="width: 15.75%;margin-right: 2%;"/></td>
                <td></td>
            </tr>
            <tr>
                <td>Total Fat</td>
                <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_2[_total_fat_am]"
                           value="<?php echo get_post_meta( $post->ID, '_total_fat_am', true ); ?>"
                           style="width: 15.75%;margin-right: 2%;"/>g</td>
                <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_2[_total_fat_dv]"
                           value="<?php echo get_post_meta( $post->ID, '_total_fat_dv', true ); ?>"
                           style="width: 15.75%;margin-right: 2%;"/>%</td>
            </tr>
            <tr>
                <td>Cholesterol</td>
                <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_2[_cholesterol_am]"
                           value="<?php echo get_post_meta( $post->ID, '_cholesterol_am', true ); ?>"
                           style="width: 15.75%;margin-right: 2%;"/>g</td>
                <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_2[_cholesterol_dv]"
                           value="<?php echo get_post_meta( $post->ID, '_cholesterol_dv', true ); ?>"
                           style="width: 15.75%;margin-right: 2%;"/>%</td>
            </tr>
            <tr>
                <td>Total Carbohydrate</td>
                <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_2[_total_carbohydrate_am]"
                           value="<?php echo get_post_meta( $post->ID, '_total_carbohydrate_am', true ); ?>"
                           style="width: 15.75%;margin-right: 2%;"/>g</td>
                <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_2[_total_carbohydrate_dv]"
                           value="<?php echo get_post_meta( $post->ID, '_total_carbohydrate_dv', true ); ?>"
                           style="width: 15.75%;margin-right: 2%;"/>%</td>
            </tr>
            <tr>
                <td>Dietary Fiber</td>
                <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_2[_dietary_fiber_am]"
                           value="<?php echo get_post_meta( $post->ID, '_dietary_fiber_am', true ); ?>"
                           style="width: 15.75%;margin-right: 2%;"/>g</td>
                <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_2[_dietary_fiber_dv]"
                           value="<?php echo get_post_meta( $post->ID, '_dietary_fiber_dv', true ); ?>"
                           style="width: 15.75%;margin-right: 2%;"/>%</td>
            </tr>
            <tr>
                <td>Sugars</td>
                <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_2[_sugars]"
                           value="<?php echo get_post_meta( $post->ID, '_sugars', true ); ?>"
                           style="width: 15.75%;margin-right: 2%;"/>g</td>
                <td>+</td>
            </tr>
            <tr>
                <td>Proteine</td>
                <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_2[_proteine]"
                           value="<?php echo get_post_meta( $post->ID, '_proteine', true ); ?>"
                           style="width: 15.75%;margin-right: 2%;"/>g</td>
                <td>+</td>
            </tr>
            <tr>
                <td>Vitamin A (as Acetate)</td>
                <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_2[_vitamin_a_am]"
                           value="<?php echo get_post_meta( $post->ID, '_vitamin_a_am', true ); ?>"
                           style="width: 15.75%;margin-right: 2%;"/>IU</td>
                <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_2[_vitamin_a_dv]"
                           value="<?php echo get_post_meta( $post->ID, '_vitamin_a_dv', true ); ?>"
                           style="width: 15.75%;margin-right: 2%;"/>%</td>
            </tr>
            <tr>
                <td>Vitamin C (Ascorbic Acid)</td>
                <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_2[_vitamin_c_am]"
                           value="<?php echo get_post_meta( $post->ID, '_vitamin_c_am', true ); ?>"
                           style="width: 15.75%;margin-right: 2%;"/>mg</td>
                <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_2[_vitamin_c_dv]"
                           value="<?php echo get_post_meta( $post->ID, '_vitamin_c_dv', true ); ?>"
                           style="width: 15.75%;margin-right: 2%;"/>%</td>
            </tr>
            <tr>
                <td>Vitamin D (as Ergocalciferol)</td>
                <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_2[_vitamin_d_am]"
                           value="<?php echo get_post_meta( $post->ID, '_vitamin_d_am', true ); ?>"
                           style="width: 15.75%;margin-right: 2%;"/>IU</td>
                <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_2[_vitamin_d_dv]"
                           value="<?php echo get_post_meta( $post->ID, '_vitamin_d_dv', true ); ?>"
                           style="width: 15.75%;margin-right: 2%;"/>%</td>
            </tr>
            <tr>
                <td>Vitamin E (as DI-Alpha Tocopherol Acetate)</td>
                <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_2[_vitamin_e_am]"
                           value="<?php echo get_post_meta( $post->ID, '_vitamin_e_am', true ); ?>"
                           style="width: 15.75%;margin-right: 2%;"/>IU</td>
                <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_2[_vitamin_e_dv]"
                           value="<?php echo get_post_meta( $post->ID, '_vitamin_e_dv', true ); ?>"
                           style="width: 15.75%;margin-right: 2%;"/>%</td>
            </tr>
            <tr>
                <td>Vitamin B6 (as Pyridoxine HCI)</td>
                <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_2[_vitamin_b6_am]"
                           value="<?php echo get_post_meta( $post->ID, '_vitamin_b6_am', true ); ?>"
                           style="width: 15.75%;margin-right: 2%;"/>mg</td>
                <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_2[_vitamin_b6_dv]"
                           value="<?php echo get_post_meta( $post->ID, '_vitamin_b6_dv', true ); ?>"
                           style="width: 15.75%;margin-right: 2%;"/>%</td>
            </tr>
            <tr>
                <td>Vitamin B12 (as Cyanocobalamin)</td>
                <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_2[_vitamin_b12_am]"
                           value="<?php echo get_post_meta( $post->ID, '_vitamin_b12_am', true ); ?>"
                           style="width: 15.75%;margin-right: 2%;"/>mcg</td>
                <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_2[_vitamin_b12_dv]"
                           value="<?php echo get_post_meta( $post->ID, '_vitamin_b12_dv', true ); ?>"
                           style="width: 15.75%;margin-right: 2%;"/>%</td>
            </tr>
            <tr>
                <td>Calcium</td>
                <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_2[_calcium_am]"
                           value="<?php echo get_post_meta( $post->ID, '_calcium_am', true ); ?>"
                           style="width: 15.75%;margin-right: 2%;"/>mg</td>
                <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_2[_calcium_dv]"
                           value="<?php echo get_post_meta( $post->ID, '_calcium_dv', true ); ?>"
                           style="width: 15.75%;margin-right: 2%;"/>%</td>
            </tr>
            <tr>
                <td>Iron</td>
                <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_2[_iron_am]"
                           value="<?php echo get_post_meta( $post->ID, '_iron_am', true ); ?>"
                           style="width: 15.75%;margin-right: 2%;"/>mg</td>
                <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_2[_iron_dv]"
                           value="<?php echo get_post_meta( $post->ID, '_iron_dv', true ); ?>"
                           style="width: 15.75%;margin-right: 2%;"/>%</td>
            </tr>
            <tr>
                <td>Sodium</td>
                <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_2[_sodium_am]"
                           value="<?php echo get_post_meta( $post->ID, '_sodium_am', true ); ?>"
                           style="width: 15.75%;margin-right: 2%;"/>mg</td>
                <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_2[_sodium_dv]"
                           value="<?php echo get_post_meta( $post->ID, '_sodium_dv', true ); ?>"
                           style="width: 15.75%;margin-right: 2%;"/>%</td>
            </tr>
            <tr>
                <td>L-Glutamine</td>
                <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_2[_glutamine]"
                           value="<?php echo get_post_meta( $post->ID, '_glutamine', true ); ?>"
                           style="width: 15.75%;margin-right: 2%;"/>g</td>
                <td>+</td>
            </tr>
            <tr>
                <td>Stevia Leaf Extract</td>
                <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_2[_stevia_leaf]"
                           value="<?php echo get_post_meta( $post->ID, '_stevia_leaf', true ); ?>"
                           style="width: 15.75%;margin-right: 2%;"/>mg</td>
                <td>+</td>
            </tr>
            <tr>
                <td>+Daily Value not established.</td>
                <td></td>
                <td></td>
            </tr>
            </tbody>
        </table>
<?php
endif;
}

