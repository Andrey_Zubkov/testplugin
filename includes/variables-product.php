<?php
add_action( 'woocommerce_product_after_variable_attributes', 'cr_variable_fields', 10, 3 );
//add_action( 'woocommerce_product_after_variable_attributes_js', 'cr_variable_fields_js' );
add_action( 'woocommerce_save_product_variation', 'art_save_variation_settings_fields', 10, 2 );

$custom_fields = [
    'Alanine'       => '_product_alanine',
    'Arginine'      => '_product_arginine',
    'Cysteine'      => '_cysteine',
    'Glutamic Acid' => '_glutamic_acid',
    'Glycine'       => '_glycine',
    'Histidine'     => '_histidine',
    'Isoleucine'    => '_isoleucine',
    'Leucine'       => '_leucine',
    'Lysine'        => '_lysine',
    'Methionine'    => '_methionine',
    'Phenylalanine' => '_phenylalanine',
    'Proline'       => '_phenylalanine',
    'Serine'        => '_serine',
    'Threonine'     => '_threonine',
    'Tryptophan'    => '_tryptophan',
    'Tyrosine'      => '_tyrosine',
    'Valine'        => '_valine',

];

function cr_variable_fields( $loop, $variation_data, $variation )
{
    global $custom_fields;
    ?>
    <h1><strong>AMINO ACID PROFILE</strong></h1>
    <table class="table table-striped">
        <thead>
        <tr>
            <th></th>
            <th><strong>Per 100g</strong></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($custom_fields as $key => $item): ?>
        <tr>
            <td><?php echo $key; ?></td>
            <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_var[<?=$variation->ID?>][<?=$item ?>]"
                       value="<?php echo get_post_meta( $variation->ID, $item, true ); ?>"
                       style="width: 15.75%;margin-right: 2%;"/>mg</td>
        </tr>
        <?php endforeach;?>
        </tbody>
    </table>
    <h1><strong>SUPPLEMENT FACTS</strong></h1>
    <p><strong>Serving Size: </strong>40.62 g(Approx. 2 Scoops)</p>
    <p><strong>Serving Per Container: </strong>24</p>
    <table class="table table-striped">
        <thead>
        <tr>
            <th></th>
            <th>Amount Per Serving</th>
            <th><strong>%DV</strong></th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>Calories</td>
            <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_var_2[<?=$variation->ID?>][_calories]"
                       value="<?php echo get_post_meta( $variation->ID, '_calories', true ); ?>"
                       style="width: 15.75%;margin-right: 2%;"/></td>
            <td></td>
        </tr>
        <tr>
            <td>Calories from Fat</td>
            <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_var_2[<?=$variation->ID?>][_calories_fat]"
                       value="<?php echo get_post_meta( $variation->ID, '_calories_fat', true ); ?>"
                       style="width: 15.75%;margin-right: 2%;"/></td>
            <td></td>
        </tr>
        <tr>
            <td>Total Fat</td>
            <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_var_2[<?=$variation->ID?>][_total_fat_am]"
                       value="<?php echo get_post_meta( $variation->ID, '_total_fat_am', true ); ?>"
                       style="width: 15.75%;margin-right: 2%;"/>g</td>
            <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_var_2[<?=$variation->ID?>][_total_fat_dv]"
                       value="<?php echo get_post_meta( $variation->ID, '_total_fat_dv', true ); ?>"
                       style="width: 15.75%;margin-right: 2%;"/>%</td>
        </tr>
        <tr>
            <td>Cholesterol</td>
            <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_var_2[<?=$variation->ID?>][_cholesterol_am]"
                       value="<?php echo get_post_meta( $variation->ID, '_cholesterol_am', true ); ?>"
                       style="width: 15.75%;margin-right: 2%;"/>g</td>
            <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_var_2[<?=$variation->ID?>][_cholesterol_dv]"
                       value="<?php echo get_post_meta( $variation->ID, '_cholesterol_dv', true ); ?>"
                       style="width: 15.75%;margin-right: 2%;"/>%</td>
        </tr>
        <tr>
            <td>Total Carbohydrate</td>
            <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_var_2[<?=$variation->ID?>][_total_carbohydrate_am]"
                       value="<?php echo get_post_meta( $variation->ID, '_total_carbohydrate_am', true ); ?>"
                       style="width: 15.75%;margin-right: 2%;"/>g</td>
            <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_var_2[<?=$variation->ID?>][_total_carbohydrate_dv]"
                       value="<?php echo get_post_meta( $variation->ID, '_total_carbohydrate_dv', true ); ?>"
                       style="width: 15.75%;margin-right: 2%;"/>%</td>
        </tr>
        <tr>
            <td>Dietary Fiber</td>
            <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_var_2[<?=$variation->ID?>][_dietary_fiber_am]"
                       value="<?php echo get_post_meta( $variation->ID, '_dietary_fiber_am', true ); ?>"
                       style="width: 15.75%;margin-right: 2%;"/>g</td>
            <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_var_2[<?=$variation->ID?>][_dietary_fiber_dv]"
                       value="<?php echo get_post_meta( $variation->ID, '_dietary_fiber_dv', true ); ?>"
                       style="width: 15.75%;margin-right: 2%;"/>%</td>
        </tr>
        <tr>
            <td>Sugars</td>
            <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_var_2[<?=$variation->ID?>][_sugars]"
                       value="<?php echo get_post_meta( $variation->ID, '_sugars', true ); ?>"
                       style="width: 15.75%;margin-right: 2%;"/>g</td>
            <td>+</td>
        </tr>
        <tr>
            <td>Proteine</td>
            <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_var_2[<?=$variation->ID?>][_proteine]"
                       value="<?php echo get_post_meta( $variation->ID, '_proteine', true ); ?>"
                       style="width: 15.75%;margin-right: 2%;"/>g</td>
            <td>+</td>
        </tr>
        <tr>
            <td>Vitamin A (as Acetate)</td>
            <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_var_2[<?=$variation->ID?>][_vitamin_a_am]"
                       value="<?php echo get_post_meta( $variation->ID, '_vitamin_a_am', true ); ?>"
                       style="width: 15.75%;margin-right: 2%;"/>IU</td>
            <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_var_2[<?=$variation->ID?>][_vitamin_a_dv]"
                       value="<?php echo get_post_meta( $variation->ID, '_vitamin_a_dv', true ); ?>"
                       style="width: 15.75%;margin-right: 2%;"/>%</td>
        </tr>
        <tr>
            <td>Vitamin C (Ascorbic Acid)</td>
            <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_var_2[<?=$variation->ID?>][_vitamin_c_am]"
                       value="<?php echo get_post_meta( $variation->ID, '_vitamin_c_am', true ); ?>"
                       style="width: 15.75%;margin-right: 2%;"/>mg</td>
            <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_var_2[<?=$variation->ID?>][_vitamin_c_dv]"
                       value="<?php echo get_post_meta( $variation->ID, '_vitamin_c_dv', true ); ?>"
                       style="width: 15.75%;margin-right: 2%;"/>%</td>
        </tr>
        <tr>
            <td>Vitamin D (as Ergocalciferol)</td>
            <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_var_2[<?=$variation->ID?>][_vitamin_d_am]"
                       value="<?php echo get_post_meta( $variation->ID, '_vitamin_d_am', true ); ?>"
                       style="width: 15.75%;margin-right: 2%;"/>IU</td>
            <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_var_2[<?=$variation->ID?>][_vitamin_d_dv]"
                       value="<?php echo get_post_meta( $variation->ID, '_vitamin_d_dv', true ); ?>"
                       style="width: 15.75%;margin-right: 2%;"/>%</td>
        </tr>
        <tr>
            <td>Vitamin E (as DI-Alpha Tocopherol Acetate)</td>
            <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_var_2[<?=$variation->ID?>][_vitamin_e_am]"
                       value="<?php echo get_post_meta( $variation->ID, '_vitamin_e_am', true ); ?>"
                       style="width: 15.75%;margin-right: 2%;"/>IU</td>
            <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_var_2[<?=$variation->ID?>][_vitamin_e_dv]"
                       value="<?php echo get_post_meta( $variation->ID, '_vitamin_e_dv', true ); ?>"
                       style="width: 15.75%;margin-right: 2%;"/>%</td>
        </tr>
        <tr>
            <td>Vitamin B6 (as Pyridoxine HCI)</td>
            <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_var_2[<?=$variation->ID?>][_vitamin_b6_am]"
                       value="<?php echo get_post_meta( $variation->ID, '_vitamin_b6_am', true ); ?>"
                       style="width: 15.75%;margin-right: 2%;"/>mg</td>
            <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_var_2[<?=$variation->ID?>][vitamin_b6_dv]"
                       value="<?php echo get_post_meta( $variation->ID, '_vitamin_b6_dv', true ); ?>"
                       style="width: 15.75%;margin-right: 2%;"/>%</td>
        </tr>
        <tr>
            <td>Vitamin B12 (as Cyanocobalamin)</td>
            <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_var_2[<?=$variation->ID?>][_vitamin_b12_am]"
                       value="<?php echo get_post_meta( $variation->ID, '_vitamin_b12_am', true ); ?>"
                       style="width: 15.75%;margin-right: 2%;"/>mcg</td>
            <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_var_2[<?=$variation->ID?>][_vitamin_b12_dv]"
                       value="<?php echo get_post_meta( $variation->ID, '_vitamin_b12_dv', true ); ?>"
                       style="width: 15.75%;margin-right: 2%;"/>%</td>
        </tr>
        <tr>
            <td>Calcium</td>
            <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_var_2[<?=$variation->ID?>][_calcium_am]"
                       value="<?php echo get_post_meta( $variation->ID, '_calcium_am', true ); ?>"
                       style="width: 15.75%;margin-right: 2%;"/>mg</td>
            <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_var_2[<?=$variation->ID?>][_calcium_dv]"
                       value="<?php echo get_post_meta( $variation->ID, '_calcium_dv', true ); ?>"
                       style="width: 15.75%;margin-right: 2%;"/>%</td>
        </tr>
        <tr>
            <td>Iron</td>
            <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_var_2[<?=$variation->ID?>][_iron_am]"
                       value="<?php echo get_post_meta( $variation->ID, '_iron_am', true ); ?>"
                       style="width: 15.75%;margin-right: 2%;"/>mg</td>
            <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_var_2[<?=$variation->ID?>][_iron_dv]"
                       value="<?php echo get_post_meta( $variation->ID, '_iron_dv', true ); ?>"
                       style="width: 15.75%;margin-right: 2%;"/>%</td>
        </tr>
        <tr>
            <td>Sodium</td>
            <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_var_2[<?=$variation->ID?>][_sodium_am]"
                       value="<?php echo get_post_meta( $variation->ID, '_sodium_am', true ); ?>"
                       style="width: 15.75%;margin-right: 2%;"/>mg</td>
            <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_var_2[<?=$variation->ID?>][_sodium_dv]"
                       value="<?php echo get_post_meta( $variation->ID, '_sodium_dv', true ); ?>"
                       style="width: 15.75%;margin-right: 2%;"/>%</td>
        </tr>
        <tr>
            <td>L-Glutamine</td>
            <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_var_2[<?=$variation->ID?>][_glutamine]"
                       value="<?php echo get_post_meta( $variation->ID, '_glutamine', true ); ?>"
                       style="width: 15.75%;margin-right: 2%;"/>g</td>
            <td>+</td>
        </tr>
        <tr>
            <td>Stevia Leaf Extract</td>
            <td><input class="input-text wc_input_decimal" size="6" type="text" name="custom_var_2[<?=$variation->ID?>][_stevia_leaf]"
                       value="<?php echo get_post_meta( $variation->ID, '_stevia_leaf', true ); ?>"
                       style="width: 15.75%;margin-right: 2%;"/>mg</td>
            <td>+</td>
        </tr>
        <tr>
            <td>+Daily Value not established.</td>
            <td></td>
            <td></td>
        </tr>
        </tbody>
    </table>

    <?php

}
function art_save_variation_settings_fields( $post_id ) {
    $woocommerce__term_prod_var = $_POST['custom_var'][ $post_id ];
    foreach ($woocommerce__term_prod_var as $key => $item) {
        if (isset($item) && ! empty( $item ) ) {
            update_post_meta( $post_id, $key, esc_attr( $item ) );
        }
    }

    $woocommerce__term_prod_var2 = $_POST['custom_var_2'][ $post_id ];
    foreach ($woocommerce__term_prod_var2 as $key => $item) {
        if (isset($item) && ! empty( $item ) ) {
            update_post_meta( $post_id, $key, esc_attr( $item ) );
        }
    }

}
