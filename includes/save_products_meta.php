<?php

add_action( 'save_post', 'wc_rrp_save_product' );
function wc_rrp_save_product( $product_id ) {
// Если это автосохранение, то ничего не делаем, сохраняем данные только при нажатии на кнопку Обновить
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
        return;
    if ( isset( $_POST['custom'] ) ) {
        foreach ( $_POST['custom'] as $key => $value ) {
            if(!empty($value)) {
                update_post_meta( $product_id, $key, $value );
            } else delete_post_meta( $product_id, $key );
        }
    }

    if ( isset( $_POST['custom_2'] ) ) {
        foreach ( $_POST['custom_2'] as $key => $value ) {
            if(!empty($value)) {
                update_post_meta( $product_id, $key, $value );
            } else delete_post_meta( $product_id, $key );
        }
    }
}