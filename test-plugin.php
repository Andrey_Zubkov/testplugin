<?php
/*
Plugin Name: Test plugin for Woocommerce product
Description: This plugin customizes products
Version: 1.0
Author: Vlad Dneprov
Author URI: http://easy-code.ru/
Plugin URI: http://easy-code.ru/lesson/building-wordpress-plugin-part-one
*/

define('PLUGIN_DIR', plugin_dir_path(__FILE__));

register_activation_hook(__FILE__, 'test_plugin_activation');
register_deactivation_hook(__FILE__, 'test_plugin_deactivation');

function test_plugin_activation() {
    if ( is_plugin_active( 'woocommerce/woocommerce.php' ) ) {

    } else {
        wp_die('You should install woocommerce plugin');
    }
}

function test_plugin_deactivation() {

}
add_action( 'admin_init', 'true_plugin_init');
require_once(PLUGIN_DIR.'includes/core.php');
require_once(PLUGIN_DIR.'includes/variables-product.php');
require_once(PLUGIN_DIR.'includes/save_products_meta.php');

function true_plugin_init() {
    wp_enqueue_style( 'main', plugins_url('assets/css/bootstrap.min.css', __FILE__) );
}
